'use strict';

/**
 * @ngdoc service
 * @name holyWeatherApp.APIService
 * @description
 * # APIService
 * Service in the holyWeatherApp.
 */
angular.module('holyWeatherApp')
  .service('APIService', function ($http) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    var API = {};

    /* forecast/city?id=524901&APPID={APIKEY} */
        var theURL = ' http://api.openweathermap.org/data/2.5/';

        API.getCityDailyWeather = function(city) {
            var params = {
                q: city || '',
                mode: 'json',
                units: 'metric',
                appid: '20ff460af7f397f82eeab13c4559b1b7'
            };
            return $http({
                method: 'GET',
                url: theURL + 'weather',
                params: params
            });
        };
        API.getCityWeatherForecast = function(city, days) {
            var params = {
                q: city || '',
                cnt: days || 0,
                mode: 'json',
                units: 'metric',
                appid: '20ff460af7f397f82eeab13c4559b1b7'
            };
            return $http({
                method: 'GET',
                url: theURL + 'forecast/daily',
                params: params
            });
        };

         API.getLocationDailyWeather = function(position) {
            var params = {
                lat: position.lat,
                lon: position.lon,
                mode: 'json',
                units: 'metric',
                appid: '20ff460af7f397f82eeab13c4559b1b7'
            };
            return $http({
                method: 'GET',
                url: theURL + 'weather',
                params: params
            });
        };
        API.getLocationWeatherForecast = function(position, days) {
            var params = {
                lat: position.lat,
                lon: position.lon,
                cnt: days || 0,
                mode: 'json',
                units: 'metric',
                appid: '20ff460af7f397f82eeab13c4559b1b7'
            };
            return $http({
                method: 'GET',
                url: theURL + 'forecast/daily',
                params: params
            });
        };

        return API;
  });
