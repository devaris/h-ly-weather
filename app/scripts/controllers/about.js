'use strict';

/**
 * @ngdoc function
 * @name holyWeatherApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the holyWeatherApp
 */
angular.module('holyWeatherApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
