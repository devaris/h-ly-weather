'use strict';

/**
 * @ngdoc function
 * @name holyWeatherApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the holyWeatherApp
 */
angular.module('holyWeatherApp')
  .controller('MainCtrl',  function($scope, $q, APIService, $localStorage) {
        $scope.city = '';
		$scope.cIcon = '';
        $scope.position = {lat:'', long:''};

        $scope.days = [];
		$scope.icons = [];

        $scope.searchCity = function() {

            if ($localStorage[$scope.city] && $localStorage[$scope.city].expire > (new Date().getTime())) {
                return $scope.loadData($scope.city);
            }

            APIService.getCityDailyWeather($scope.city).then(function(response) {
                if (+response.data.cod === 200) {
                    $scope.currentData = true;
                    $scope.currentCityWF = response.data;
					$scope.cIcon = $scope.getNewIcon($scope.currentCityWF.weather[0].icon);
                    document.querySelector("body").className += " bgPurple";
                    document.querySelector(".headerLogo > img").className = "hidden";
                    document.querySelector(".headerLogo > img + img").className = "";
                } else if (response.data.cod === 404) {
                    $scope.currentData = false;
                    $scope.cityWF = response.data.message;
                }
            });

            var daysEstimation = 5;

            APIService.getCityWeatherForecast($scope.city, daysEstimation).then(function(response) {
                if (+response.data.cod === 200) {
                    $scope.currentDataNextF = true;
                    $scope.nextWF = response.data;
					var cTime = new Date();
					for (var i=0; i<daysEstimation; i++){
						var temp = cTime.getTime() + (24 * 60 * 60 * 1000);
						cTime = new Date(temp);
						$scope.days.push(cTime);
						$scope.icons.push($scope.getNewIcon($scope.nextWF.list[i].weather[0].icon));
					}
                    document.querySelector("body").className += " bgPurple";
                    document.querySelector(".headerLogo > img").className = "hidden";
                    document.querySelector(".headerLogo > img + img").className = "";
                } else if (response.data.cod === 404) {
                    $scope.currentDataNextF = false;
                    $scope.nextWF = response.data.message;
                }
            });

            if ($scope.currentData && $scope.currentDataNextF) {
                $scope.saveData($scope.city);
            }

        };

		$scope.getNewIcon = function(theIcon) {
			var theNewIcon = "B";
			switch (theIcon){
				case "01d":
					theNewIcon = "B";
					break;
				case "01n":
					theNewIcon = "C";
					break;
				case "02d":
					theNewIcon = "H";
					break;
				case "02n":
					theNewIcon = "I";
					break;
				case "03d":
				case "03n":
					theNewIcon = "N";
					break;
				case "04d":
				case "04n":
					theNewIcon = "Y";
					break;
				case "09d":
				case "09n":
					theNewIcon = "Q";
					break;
				case "10d":
				case "10n":
					theNewIcon = "R";
					break;
				case "11d":
				case "11n":
					theNewIcon = "Z";
					break;
				case "13d":
				case "13n":
					theNewIcon = "W";
					break;
				case "50d":
				case "50n":
					theNewIcon = "M";
					break;
			}
			return theNewIcon;
		};

        function showLocation(position) {
            $scope.position.lat = position.coords.latitude;
            $scope.position.lon = position.coords.longitude;
            //alert("Latitude : " + $scope.position.lat + " Longitude: " + $scope.position.lon);

            if ($localStorage[$scope.city] && $localStorage[$scope.city].expire > (new Date().getTime())) {
                return $scope.loadData($scope.city);
            }

            APIService.getLocationDailyWeather($scope.position).then(function(response) {
                if (+response.data.cod === 200) {
                    $scope.currentData = true;
                    $scope.currentCityWF = response.data;
					$scope.cIcon = $scope.getNewIcon($scope.currentCityWF.weather[0].icon);
                    document.querySelector("body").className += " bgPurple";
                    document.querySelector(".headerLogo > img").className = "hidden";
                    document.querySelector(".headerLogo > img + img").className = "";
                } else if (response.data.cod === 404) {
                    $scope.currentData = false;
                    $scope.cityWF = response.data.message;
                }
            });

            var daysEstimation = 5;

            APIService.getLocationWeatherForecast($scope.position, daysEstimation).then(function(response) {
                if (+response.data.cod === 200) {
                    $scope.currentDataNextF = true;
                    $scope.nextWF = response.data;
					var cTime = new Date();
					for (var i=0; i<daysEstimation; i++){
						var temp = cTime.getTime() + (24 * 60 * 60 * 1000);
						cTime = new Date(temp);
						$scope.days.push(cTime);
						$scope.icons.push($scope.getNewIcon($scope.nextWF.list[i].weather[0].icon));
					}
                    document.querySelector("body").className += " bgPurple";
                    document.querySelector(".headerLogo > img").className = "hidden";
                    document.querySelector(".headerLogo > img + img").className = "";
                } else if (response.data.cod === 404) {
                    $scope.currentDataNextF = false;
                    $scope.nextWF = response.data.message;
                }
            });

            if ($scope.currentData && $scope.currentDataNextF) {
                $scope.saveData($scope.city);
            }
         }

         function errorHandler(err) {
            if(err.code == 1) {
               alert("Error: Access is denied!");
            }

            else if( err.code == 2) {
               alert("Error: Position is unavailable!");
            }
         }

         $scope.searchLocation = function() {
             if(navigator.geolocation){
               // timeout at 60000 milliseconds (60 seconds)
               var options = {timeout:60000};
               navigator.geolocation.getCurrentPosition(showLocation, errorHandler, options);
            }

            else{
               alert("Sorry, browser does not support geolocation!");
            }
         };

        $scope.loadData = function(currentCity) {
            $scope.currentCityWF = $localStorage[currentCity].daily;
            $scope.nextWF = $localStorage[currentCity].weekly;
        };

        $scope.saveData = function(newCity) {
            $localStorage[newCity] = {};
            $localStorage[newCity].daily = $scope.currentCityWF;
            $localStorage[newCity].weekly = $scope.nextWF;
            $localStorage[newCity].expire = new Date().getTime() + 360000;
        };

        // SEO REQUIREMENT:
        // PhantomJS pre-rendering workflow requires the page to declare, through htmlReady(), that
        // we are finished with this controller.
        $scope.htmlReady();

    });
