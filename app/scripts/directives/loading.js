'use strict';

/**
 * @ngdoc directive
 * @name holyWeatherApp.directive:loading
 * @description
 * # loading
 */
angular.module('holyWeatherApp')
  .directive('loading', ['$http' ,function ($http) {
    return {
      template: '<div class="loading"><img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA0OTggNDk4IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA0OTggNDk4OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjEyOHB4IiBoZWlnaHQ9IjEyOHB4Ij4KPGc+Cgk8Zz4KCQk8cGF0aCBkPSJNMjU3LDMyLjJWMGgtMTZ2MzIuMkMxMzQuNDE2LDM2LjQyNCw0OSwxMjQuNCw0OSwyMzJoMTZjMC0yMi4wNTYsMTcuOTQ0LTQwLDQwLTQwYzIyLjA1NiwwLDQwLDE3Ljk0NCw0MCw0MGgxNiAgICBjMC0yMi4wNTYsMTcuOTQ0LTQwLDQwLTQwYzIyLjA1NiwwLDQwLDE3Ljk0NCw0MCw0MHYyMDRjMCwzMy4wODgsMjYuOTEyLDYyLDYwLDYyczYwLTI2LDYwLTU4aC0xNmMwLDI0LTE5LjczNiw0Mi00NCw0MiAgICBjLTI0LjI2NCwwLTQ0LTIxLjczNi00NC00NlYyMzJjMC0yMi4wNTYsMTcuOTQ0LTQwLDQwLTQwYzIyLjA1NiwwLDQwLDE3Ljk0NCw0MCw0MGgxNmMwLTIyLjA1NiwxNy45NDQtNDAsNDAtNDAgICAgYzIyLjA1NiwwLDQwLDE3Ljk0NCw0MCw0MGgxNkM0NDksMTI0LjQsMzYzLjU4NCwzNi40MjQsMjU3LDMyLjJ6IE0xNDYuODg4LDE5NS4wNEMxMzYuNjI0LDE4My40MTYsMTIxLjY4OCwxNzYsMTA1LDE3NiAgICBjLTEzLjA5NiwwLTI1LjE2LDQuNTItMzQuNzA0LDEyLjA4YzE3Ljc5Mi03Mi4zNiw3OC41NTItMTI3Ljg3MiwxNTMuNTM2LTEzOC4yQzE4MS40NjQsODcuMjMyLDE1NC4wMDgsMTM5LjE4NCwxNDYuODg4LDE5NS4wNHogICAgIE0yOTcsMTc2Yy0yMC4zNDQsMC0zOC4xOTIsMTAuOTA0LTQ4LDI3LjE3NkMyMzkuMTkyLDE4Ni45MDQsMjIxLjM0NCwxNzYsMjAxLDE3NmMtMTQuMzIsMC0yNy4zMzYsNS40OC0zNy4yNDgsMTQuMzYgICAgQzE3Mi41MiwxMzUuNCwyMDIuMzY4LDg0Ljk2OCwyNDcuNCw1MS4yTDI0OSw1MGwxLjYsMS4yYzQ1LjAzMiwzMy43NjgsNzQuODgsODQuMjA4LDgzLjY0OCwxMzkuMTYgICAgQzMyNC4zMzYsMTgxLjQ4LDMxMS4zMiwxNzYsMjk3LDE3NnogTTM5MywxNzZjLTE2LjY4OCwwLTMxLjYyNCw3LjQxNi00MS44ODgsMTkuMDRjLTcuMTItNTUuODU2LTM0LjU3Ni0xMDcuODA4LTc2Ljk0NC0xNDUuMTYgICAgYzc0Ljk4NCwxMC4zMiwxMzUuNzQ0LDY1Ljg0LDE1My41MzYsMTM4LjJDNDE4LjE2LDE4MC41Miw0MDYuMDk2LDE3NiwzOTMsMTc2eiIgZmlsbD0iIzhiM2QzZCIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" /><div>umbrellapp</div><div class="uil-rolling-css" style="transform:scale(0.3);"><div><div></div><div></div></div></div></div>',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
       	scope.isLoading = function () {
			return $http.pendingRequests.length > 0;
		};

		scope.$watch(scope.isLoading, function (v)
		{
			if(v){
				element.show();
			}else{
				element.hide();
			}
		});
      }
    };
  }]);
